﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallSaucer : SaucerBase
{
	protected override void InitialBehaviour()
	{
        StartCoroutine(MovingTowardsPlayer());
        StartCoroutine(ShootAtPlayer());
    }

	IEnumerator ShootAtPlayer()
	{
		while(true)
		{
            yield return new WaitForSeconds(m_FireRate);
			Vector2 direction = (Player.instance.transform.position - transform.position);
			transform.up = direction;
            PoolSystem.instance.SpawnBullet("SaucerBullet", gameObject);
        }
	}
}

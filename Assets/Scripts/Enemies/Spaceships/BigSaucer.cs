﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigSaucer : SaucerBase
{
    [SerializeField] private float m_AngularSpeed = 0;
	
	protected override void InitialBehaviour()
	{
        StartCoroutine(MovingTowardsPlayer());
        StartCoroutine(Shoot());
    }

	void Update()
	{
        transform.Rotate(0, 0, m_AngularSpeed * Time.deltaTime);
    }

	IEnumerator Shoot()
	{
		while(true)
		{
            yield return new WaitForSeconds(m_FireRate);
            PoolSystem.instance.SpawnBullet("SaucerBullet", gameObject);
            yield return null;
        }
	}

	
}

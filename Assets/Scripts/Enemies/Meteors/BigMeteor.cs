﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigMeteor : MeteorBase
{
	protected override IEnumerator Die() 
	{
        for (int i = 0; i < m_PiecesAfterDie; i++)
        {
            PoolSystem.instance.SpawnMeteor("MediumMeteor", this.gameObject ,1);
            yield return null;
        }
        gameObject.SetActive(false);
    }
	
}

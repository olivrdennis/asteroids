﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallMeteor : MeteorBase 
{
	protected override IEnumerator Die() 
	{
		gameObject.SetActive(false);
        yield return null;
    }
	
}

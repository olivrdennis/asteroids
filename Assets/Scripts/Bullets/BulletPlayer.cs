﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlayer : BulletBase
{
	protected override void OnCollisionEnter2D(Collision2D other)
	{
        EnemyBase enemy = other.transform.GetComponent<EnemyBase>();
        if(enemy != null)
        {
            enemy.TakeDamage(m_Damage);
            DestroyBullet();
        }
    }
	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSaucer : BulletBase 
{
	protected override void OnCollisionEnter2D(Collision2D other)
	{
        Player player = other.transform.GetComponent<Player>();
		if(player != null)
		{
			player.TakeDamage(m_Damage);
            DestroyBullet();
        }
    }
}

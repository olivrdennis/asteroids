﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public abstract class BulletBase : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] protected float m_Speed = 0;
    [SerializeField] protected float m_LifeTime = 0;
    [SerializeField] protected float m_Damage = 0;

    void OnEnable()
    {
        StartCoroutine(SelfDestroy());
    }

    void Update() 
	{
        transform.Translate(Vector2.up * m_Speed * Time.deltaTime);
    }

    IEnumerator SelfDestroy()
    {
        yield return new WaitForSeconds(m_LifeTime);
        gameObject.SetActive(false);
    }

	protected virtual void DestroyBullet()
	{
        gameObject.SetActive(false);
    }

    protected abstract void OnCollisionEnter2D(Collision2D other);

}

﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public abstract class EnemyBase : MonoBehaviour 
{
    public static Action EnemyDeathEvent;
    public static Action<int> ComputeScoreEvent;

    [Header("Properties")]
    [SerializeField] protected float m_Life = 0;
    [SerializeField] protected int m_WorthPoints = 0;
    protected float m_PlayerDamage = 0;
    protected bool m_IsAlive = true;
    protected Rigidbody2D m_RigidBody;
    protected SpriteRenderer m_Renderer;

    void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Renderer = GetComponent<SpriteRenderer>();
    }

    void OnEnable()
    {
        InitialBehaviour();
        m_IsAlive = true;
    }

    public virtual void TakeDamage(float damage)
    {
        m_Life -= damage;

        if(m_Life <= 0) 
        {
            PoolSystem.instance.SpawnBullet("Explosion", gameObject);
            StartCoroutine(Die());
            EnemyDeathEvent();
            ComputeScoreEvent(m_WorthPoints);
        } 
    }

    protected virtual IEnumerator DestroyWhenOffScreen()
    {
        yield return new WaitForSeconds(5f);
        if(!this.m_Renderer.isVisible) 
        {
            this.gameObject.SetActive(false);
            EnemyDeathEvent();
        }
    }

    //Screen Warp
    public void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("MainCamera"))
        {
            transform.position = (-transform.position);
            StartCoroutine(DestroyWhenOffScreen());
        } 
    }

    abstract protected IEnumerator Die();
    abstract protected void InitialBehaviour();

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SaucerBase : EnemyBase
{
	[Header("Movement")]
    [SerializeField] protected float m_ImpulseForce = 0;
    [SerializeField] protected float m_TimeToMove = 0;
	[Header("Combat")]
    [SerializeField] protected float m_FireRate = 0;

    public override void TakeDamage(float damage)
    {
        m_Life -= damage;

        if(m_Life <= 0) 
        {
            PoolSystem.instance.SpawnBullet("Explosion", gameObject);
            StartCoroutine(Die());
            ComputeScoreEvent(m_WorthPoints);
        } 
    }

    protected override IEnumerator DestroyWhenOffScreen()
    {
        yield return new WaitForSeconds(5f);
        if(!m_Renderer.isVisible) 
        {
            this.gameObject.SetActive(false);
        }
    }

	protected IEnumerator MovingTowardsPlayer()
	{
		while(true)
		{
            Vector2 direction = (Player.instance.transform.position - transform.position).normalized;
            m_RigidBody.velocity = Vector2.zero;
            m_RigidBody.AddRelativeForce(new Vector2(direction.x + Random.Range(1f,-1f), direction.y + Random.Range(-1f,1f)) * Time.deltaTime * m_ImpulseForce, ForceMode2D.Impulse);
            yield return new WaitForSeconds(m_TimeToMove);
        }
	}

	protected override IEnumerator Die()
	{
        gameObject.SetActive(false);
        yield return null;
    }
}

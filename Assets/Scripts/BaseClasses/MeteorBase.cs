﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MeteorBase : EnemyBase
{
    [Header("Properties")]
    [SerializeField] protected float m_Damage = 0;
    [Header("Movement")]
    [SerializeField] protected float m_InitialForce = 0;
    [Header("Destruction")]
    [SerializeField] protected float m_PiecesAfterDie = 0;

    protected void OnCollisionEnter2D(Collision2D other)
	{
        Player player = other.transform.GetComponent<Player>();
		if(player != null)
		{
			player.TakeDamage(m_Damage);
        }
    }

    protected override void InitialBehaviour() 
	{
        Vector2 direction = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        m_RigidBody.AddRelativeForce(direction * Time.deltaTime * m_InitialForce, ForceMode2D.Impulse);
    }

    protected abstract override IEnumerator Die();


}

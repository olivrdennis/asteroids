﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Player : MonoBehaviour 
{
    public static Action DamageEvent;
    public static Action GameOverEvent;

    public static Player instance;

    [Header("Properties")]
    [SerializeField] private int m_MaxLife = 0;
    [SerializeField] private float m_StartLife = 0;
    public float Life 
    {
        get { return m_StartLife; }
    }
    [Header("Movement")]
    [SerializeField] private float m_MoveSpeed = 0;
    [SerializeField] private float m_AngularSpeed = 0;
    private float m_HorizontalAxis = 0;
    private bool m_CanThrust = false;
    private bool m_CanRotate = false;
    private bool m_CanShoot = false;
    private bool m_CanGetHit = true;

    [Header("Combat")]
    [SerializeField] private float m_InvulnerabilityTime = 0;
    [SerializeField] private float m_FireRate = 0;
    [SerializeField] private float m_Damage = 0;
    public float Damage 
    {
        get { return m_Damage; }
    }

    [Header("Thrust Particle")]
    [SerializeField] private ParticleSystem m_ThrustParticle;

    private Rigidbody2D m_RigidBody;
    private Animator m_Animator;
    private AudioSource m_Audio;

    void OnEnable()
    {
        SpawnController.ExtraLifeEvent += ReceiveExtraLife;
    }

    void OnDisable()
    {
        SpawnController.ExtraLifeEvent -= ReceiveExtraLife;
    }

    void Awake()
    {
        instance = this;

        m_Animator = GetComponent<Animator>();
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Audio = GetComponent<AudioSource>();
    }

    void Start()
    {
        StartCoroutine(Shoot());
    }
	
	void FixedUpdate()
	{
		if(m_CanThrust) Thrust();
		if(m_CanRotate) RotateShip();
    }
	
	void Update ()
	{
        #region "Movement"
        //Thrust forward	
        if(Input.GetKey(KeyCode.UpArrow)) 
        {
            m_CanThrust = true;
            m_Audio.volume = 1;
            var emission = m_ThrustParticle.emission;
            emission.rateOverDistance = 100;
        }
		else 
        {
            m_CanThrust = false;
            m_Audio.volume = 0;;
            var emission = m_ThrustParticle.emission;
            emission.rateOverDistance = 0;
        }

        //HyperSpace
        if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            Vector2 randomPos = Camera.main.ScreenToWorldPoint(new Vector2(UnityEngine.Random.Range(0, Screen.width), UnityEngine.Random.Range(0, Screen.height)));
            transform.position = new Vector3(randomPos.x, randomPos.y, 0);
        }       

        //Rotate
		if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
            m_CanRotate = true;
		else
            m_CanRotate = false;
        	
        #endregion

    }

    public void TakeDamage(float damage)
    {
        if(m_CanGetHit)
        {
            DamageEvent();
            CameraShake.instance.ShakeScreen();
            m_Animator.SetTrigger("GetHit");
            m_StartLife -= damage;
            StartCoroutine(Invulnerability());

            if(m_StartLife <= 0)
            {
                StartCoroutine(Die());
                GameOverEvent();
            }         
        }
       
    }

    IEnumerator Shoot()
    {
        while(true)
        {
            if(Input.GetKey(KeyCode.Space) || Input.GetKeyDown(KeyCode.Space)) 
            {
                if(m_CanShoot) 
                {
                    PoolSystem.instance.SpawnBullet("PlayerBullet", transform.gameObject);
                    m_CanShoot = false;
                } 
            }

            if (!m_CanShoot) 
            {
                yield return new WaitForSeconds(m_FireRate);
                m_CanShoot = true;
            }
            yield return null;
        }
    }

    IEnumerator Invulnerability()
    {
        m_CanGetHit = false;
        yield return new WaitForSeconds(m_InvulnerabilityTime);
        m_CanGetHit = true;
    }

    IEnumerator Die()
    {
        PoolSystem.instance.SpawnBullet("Explosion", gameObject);
        gameObject.SetActive(false);
        yield return null;
    }

    //Screen Warp
    public void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("MainCamera")) transform.position = (-transform.position);
    }

    private void ReceiveExtraLife()
    {
        if(m_StartLife < m_MaxLife) m_StartLife++;
    }

	private void Thrust()
	{
		m_RigidBody.AddRelativeForce(Vector2.up * m_MoveSpeed * Time.deltaTime, ForceMode2D.Force);
	}

	private void RotateShip()
	{
        m_HorizontalAxis = Input.GetAxis("Horizontal");
        transform.Rotate(0, 0,m_HorizontalAxis * m_AngularSpeed * Time.deltaTime);
    }

}

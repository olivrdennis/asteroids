﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

    public static CameraShake instance;

    [SerializeField] private float m_Power = 0.1f;
    [SerializeField] private float m_Duration = .5f;
    [SerializeField] private float m_SlowDownAmount = 1.0f;

    private new Transform camera;
    private Vector3 startPosition;
    private float initialm_Duration = 0.5f;

    private bool m_shouldShake = false;
    public bool ShouldShake 
    {
        get { return m_shouldShake; }
    }
    
    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        Player.DamageEvent += ShakeScreen;
    }

    void OnDisable()
    {
        Player.DamageEvent -= ShakeScreen;
    }

	void Start ()
    {
        camera = Camera.main.transform;
        startPosition = camera.localPosition;
        initialm_Duration = m_Duration;
	}
	
	// Update is called once per frame
	void Update ()
    {
        startPosition = camera.localPosition;
        if (m_shouldShake)
        {
            if(m_Duration > 0)
            {
                camera.localPosition = startPosition + Random.insideUnitSphere * m_Power;
                m_Duration -= Time.deltaTime * m_SlowDownAmount;
            }
            else
            {
                m_shouldShake = false;
                m_Duration = initialm_Duration;
                camera.localPosition = startPosition;
                StartCoroutine(ResetPosition());
            }
        }

        if(m_shouldShake && transform.position != new Vector3(0,0,-10)) StopCoroutine(ResetPosition());
    }

    public void ShakeScreen()
    {
        m_shouldShake = true;
    }

    IEnumerator ResetPosition()
    {
        yield return new WaitForSeconds(2f);

        while(transform.position != new Vector3(0,0,-10)) 
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(0, 0, -10), Time.deltaTime);
            yield return null;
        }
        
    }


}

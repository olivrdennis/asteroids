﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageScreen : MonoBehaviour 
{
    [SerializeField] private Animator m_Animator;
    [SerializeField] private AudioSource m_Audio;

    void OnEnable()
	{
        Player.DamageEvent += DamageFeedback;
    }

	void OnDisable()
	{
        Player.DamageEvent -= DamageFeedback;
    }

	void DamageFeedback()
	{
        m_Animator.SetTrigger("TakeDamage");
        m_Audio.Play();
    }
	
}

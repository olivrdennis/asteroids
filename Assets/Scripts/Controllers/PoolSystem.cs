﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class ObjectPoolItem
{
    public int numberOfObjects;
    public GameObject genericObject;
}


public class PoolSystem : MonoBehaviour
{
    public static PoolSystem instance;
    public static float EnemiesAlive = 0;

    public List<ObjectPoolItem> m_ItemsToPool;
    List<GameObject> m_PooledObjects;

    void OnEnable()
    {
        EnemyBase.EnemyDeathEvent += RemoveActiveEnemie;
    }

    void OnDisable()
    {
        EnemyBase.EnemyDeathEvent -= RemoveActiveEnemie;
    }
    
    void Awake()
	{
        instance = this;
    }

    void Start()
    {
        CreateObjectPool();
    }

    ///<summary>
    ///Spawns an object at a given position.
    ///<param name="spawnMode">1 = local position 2 = Random position on screen</param>  
    ///</summary>
    public void SpawnEnemy(string enemyTag, GameObject objectReference, int spawnMode) 
    {
        var pooledObj = GetPooledObject(enemyTag);

        if(pooledObj != null) 
        {
            if(spawnMode == 1) 
            {
                pooledObj.transform.position = objectReference.transform.position;
                pooledObj.transform.rotation = objectReference.transform.rotation;
            }

            if(spawnMode == 2) 
            {
                Vector2 randomPos = Camera.main.ScreenToWorldPoint(new Vector2(UnityEngine.Random.Range(0, Screen.width), UnityEngine.Random.Range(0, Screen.height)));
                pooledObj.transform.position = randomPos;
                pooledObj.transform.rotation = Quaternion.identity;
            }

            pooledObj.SetActive(true);
        }
        
    }

    public void SpawnMeteor(string enemyTag, GameObject objectReference, int spawnMode) 
    {
        var pooledObj = GetPooledObject(enemyTag);

        if(pooledObj != null) 
        {
            if(spawnMode == 1) 
            {
                pooledObj.transform.position = objectReference.transform.position;
                pooledObj.transform.rotation = objectReference.transform.rotation;
            }

            if(spawnMode == 2) 
            {
                Vector2 randomPos = Camera.main.ScreenToWorldPoint(new Vector2(UnityEngine.Random.Range(0, Screen.width), UnityEngine.Random.Range(0, Screen.height)));
                pooledObj.transform.position = randomPos;
                pooledObj.transform.rotation = Quaternion.identity;
            }

            pooledObj.SetActive(true);
            EnemiesAlive++;
        }
        
    }

    ///<summary>
    ///Spawns an object at a given position.
    ///</summary>
    public void SpawnBullet(string bulletTag,GameObject objectReference) 
    {
        var pooledObj = GetPooledObject(bulletTag);
        if(pooledObj != null)
        {
            pooledObj.transform.position = objectReference.transform.position;
            pooledObj.transform.rotation = objectReference.transform.rotation;
            pooledObj.SetActive(true);
        }
    }
    
    public void RemoveActiveEnemie()
    {
        EnemiesAlive--;
    }

    public GameObject GetPooledObject(string tag)
    {
        //Pegando objeto inativo na Hierarquia baseado em sua TAG
        if(tag != null) 
        {
            for (int i = 0; i < m_PooledObjects.Count; i++)
            {
                if (!m_PooledObjects[i].activeInHierarchy && m_PooledObjects[i].tag == tag) 
                {
                    return m_PooledObjects[i];
                }
            }
        }
        return null;
        
    }

    void CreateObjectPool()
    {
        m_PooledObjects = new List<GameObject>();
        foreach (ObjectPoolItem item in m_ItemsToPool)
        {
            for (int i = 0; i < item.numberOfObjects; i++)
            {
                GameObject obj = (GameObject)Instantiate(item.genericObject);
                obj.SetActive(false);
                m_PooledObjects.Add(obj);
            }
        }

    }



    

}

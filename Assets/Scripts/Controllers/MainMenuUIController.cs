﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenuUIController : MonoBehaviour {


	[Header("Audio")]
	[SerializeField] private AudioSource m_Audio;
	
    [Header("MainMenu References")]
    [SerializeField] private GameObject m_LoadingPanel;
    [SerializeField] private GameObject m_HowToPlayPanel;


	#region "Button Functions"
	public void Back()
	{
        m_HowToPlayPanel.SetActive(false);
    }

	public void HowToPlay()
	{
        m_HowToPlayPanel.SetActive(true);
    }

	public void StartPlay()
	{
        StartCoroutine(Play());
    }

	public IEnumerator Play()
	{
        Time.timeScale = 1;
        m_LoadingPanel.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadSceneAsync("MainScene");
        yield return null;
    }
	#endregion
}

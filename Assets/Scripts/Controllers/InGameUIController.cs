﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class InGameUIController : MonoBehaviour
{
    [Header("Animation")]
    [SerializeField] private Animator m_Animator;
    [SerializeField] private AudioSource m_Audio;
    [SerializeField] private AudioClip m_HighScoreFanfare;
    [SerializeField] private AudioClip m_ComputePoints;

    [Header("PauseScreen")]
    [SerializeField] private GameObject m_PausePanel;

    [Header("HUD")]
    [SerializeField] private GameObject m_HUDPanel;
    [SerializeField] private Text m_Life;
    [SerializeField] private Text m_Wave;
    [SerializeField] private Text m_EnemiesLeft;
    [SerializeField] private Text m_Score;

    [Header("GameOverScreen")]
    [SerializeField] private GameObject m_GameOverPanel;
    [SerializeField] private GameObject m_CurrentScoreLabel;
    [SerializeField] private Text m_CurrentScore;
    [SerializeField] private GameObject m_HighScoreLabel;
    [SerializeField] private Text m_HighScore;
    [SerializeField] private GameObject m_Buttons;

    void Start()
    {
        SpawnController.HighScore = PlayerPrefs.GetInt("HighScore", 0);
        SpawnController.CurrentWave = 0;
        PoolSystem.EnemiesAlive = 0;
        Time.timeScale = 1;
    }

    void OnEnable()
    {
        Player.GameOverEvent += StartGameOver;
    }

    void OnDisable()
    {
        Player.GameOverEvent -= StartGameOver;
    }

    void Update()
	{
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(m_PausePanel.activeInHierarchy) 
            {
                Time.timeScale = 1;
                m_PausePanel.SetActive(false);
            }
            else 
            {
                Time.timeScale = 0;
                m_PausePanel.SetActive(true);
            }
        }


		//Updating values on screen.
        m_Life.text = Player.instance.Life.ToString();
        m_Wave.text = SpawnController.CurrentWave.ToString();
        m_EnemiesLeft.text = PoolSystem.EnemiesAlive.ToString();
        m_Score.text = SpawnController.Score.ToString();
    }

    void StartGameOver()
    {
        StartCoroutine(GameOver());
    }

    IEnumerator GameOver()
    {
        
        m_HUDPanel.SetActive(false);
        m_HighScore.text = SpawnController.HighScore.ToString();
        //Activate gameover panel.
        yield return new WaitForSeconds(1);
        m_GameOverPanel.SetActive(true);

        int counter = 0;
        //Activate CurrentScore Label.
        yield return new WaitForSeconds(0.5f);
        m_CurrentScoreLabel.SetActive(true);

        //Compute points in a cool way.
        while(counter < SpawnController.Score)
        {
            m_Audio.clip = m_ComputePoints;
            if((counter + SpawnController.Score / 30) >= SpawnController.Score) 
            {
                counter = (int)SpawnController.Score;
                m_Audio.Play();
            }
            else
            {
                counter += (int)SpawnController.Score / 30;
                m_Audio.Play();
            }
            m_CurrentScore.text = counter.ToString();
            yield return null;
        }

        
        yield return new WaitForSeconds(1);
        m_HighScoreLabel.SetActive(true);
        if(counter > SpawnController.HighScore) 
        {
            m_Audio.clip = m_HighScoreFanfare;
            m_Audio.Play();
            m_Animator.SetTrigger("HighScore");
            m_HighScore.text = counter.ToString();
            PlayerPrefs.SetInt("HighScore", counter);
        }
        SpawnController.Score = 0;
        

        m_Buttons.SetActive(true);

    }

    #region ButtonFunctions

    public void Retry()
    {
        SceneManager.LoadSceneAsync("MainScene");
    }

    public void Resume()
    {
        Time.timeScale = 1;
        m_PausePanel.SetActive(false);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Exit()
    {
        Application.Quit();
    }
    #endregion
}

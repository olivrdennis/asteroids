﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Dificulty 
{
    public string name;
    public int meteorsPerWave;
    public int scoreToChange;
    public float spaceShipSpawnRate;
}

public class SpawnController : MonoBehaviour
{
    public static Action ExtraLifeEvent;

    public static int Score = 0;
    public static int HighScore = 0;
    public static int CurrentWave = 0;

    [Header("Audio")]
    [SerializeField] private AudioSource m_Audio;
    [SerializeField] private AudioClip m_ExtraLife;
    [Header("Progression Settings")]
    [SerializeField] private int m_ScoreToExtraLife = 0;
    private int m_ExtraLifeScore = 0;
    private int m_DificultyIndex = 0;
    [SerializeField] private int m_ScoreToSpawnOnlySaucer = 0;
    [Header("Wave Settings")]
    [SerializeField] public float m_TimeBetweenWaves;
    [SerializeField] private float m_TimeToStartSpawn = 0;
    [SerializeField] private List<Dificulty> m_Dificulties;

    void OnEnable()
    {
        EnemyBase.ComputeScoreEvent += ComputeScore;
    }

    void OnDisable()
    {
        EnemyBase.ComputeScoreEvent -= ComputeScore;
    }

    void Start()
	{
        StartCoroutine(StartSpawn());
        StartCoroutine(StartSpawnSpaceShips());
    }

    void ComputeScore(int score)
    {
        Score += score;

        m_ExtraLifeScore += score;
        if(m_ExtraLifeScore > m_ScoreToExtraLife)
        {
            m_Audio.clip = m_ExtraLife;
            m_Audio.Play();
            ExtraLifeEvent();
            m_ExtraLifeScore = 0;
        }
    }


	IEnumerator StartSpawn()
	{
        m_DificultyIndex = 0;

        while(true)
        {
            yield return new WaitForSeconds(m_TimeToStartSpawn);
            CurrentWave++;
            if(SpawnController.Score >= m_Dificulties[m_DificultyIndex].scoreToChange) 
            {
                if(m_DificultyIndex < m_Dificulties.Count -1) m_DificultyIndex++;
            }
            //Spawning meteors.
            for (int i = 0; i < m_Dificulties[m_DificultyIndex].meteorsPerWave; i++)
            {
                PoolSystem.instance.SpawnMeteor("BigMeteor", gameObject, 2);
            }

            while(PoolSystem.EnemiesAlive > 0)
			{
                yield return null;
            }    
            yield return new WaitForSeconds(m_TimeBetweenWaves);
        }     
    

    }

    IEnumerator StartSpawnSpaceShips()
    {
        List<string> enemies = new List<string> { "BigSaucer", "SmallSaucer" };
        while(true)
        {
            yield return new WaitForSeconds(m_Dificulties[m_DificultyIndex].spaceShipSpawnRate);

            int chooser = UnityEngine.Random.Range(0, 2);
            if(SpawnController.Score > m_ScoreToSpawnOnlySaucer) 
            {
                //Spawns only small saucers.
                chooser = 1;
            }
            PoolSystem.instance.SpawnEnemy(enemies[chooser], gameObject, 2);
        }
    }

}


